MSc Thesis: Learning contractive optimisation rules
========================

This repo contains the code to replicate the results of my MSc thesis.

The shell scripts (e.g. thesis/run_basic_models.sh) can be used to train the learned
optimisers. The filenames that have a corresponding Python and Julia file (e.g.
loss_comparison.py and loss_comparison.jl) are meant to be run one after
another. The Python script produces the output and the Julia script plots the
output.

The Python dependencies are listed in the Pipfile. The Julia scripts require the
following three libraries: CSV, Plots, LaTeXStrings.
