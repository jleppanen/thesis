
mkdir -p results/quad/
python3 quadratic_experiment.py --structured 1 --hg-mode fixed_point --dataset quad --opt crnn --dim 10 --reg-param 0.5  --ntasks 5000 --T 100 --constraint strict
python3 quadratic_experiment.py --structured 1 --hg-mode fixed_point --dataset quad --opt crnn --dim 10 --reg-param 0.5  --ntasks 5000 --componentwise 1 --T 100 --constraint strict
python3 quadratic_experiment.py --structured 1 --hg-mode fixed_point --dataset quad --opt sd --dim 10 --reg-param 0.5  --ntasks 5000 --T 100 --constraint strict
python3 quadratic_experiment.py --structured 1 --hg-mode fixed_point --dataset quad --opt sd --dim 10 --reg-param 0.5  --ntasks 5000 --componentwise 1 --T 100 --constraint strict
python3 loss_comparison.py --dataset quad --dim 10 --reg-param 0.5  --T 100 --constraint strict --structured 1

python3 quadratic_experiment.py  --hg-mode fixed_point --dataset quad --opt crnn --dim 10 --reg-param 0.5  --ntasks 5000 --T 100 --constraint strict
python3 quadratic_experiment.py  --hg-mode fixed_point --dataset quad --opt crnn --dim 10 --reg-param 0.5  --ntasks 5000 --componentwise 1 --T 100 --constraint strict
python3 quadratic_experiment.py  --hg-mode fixed_point --dataset quad --opt sd --dim 10 --reg-param 0.5  --ntasks 5000 --T 100 --constraint strict
python3 quadratic_experiment.py  --hg-mode fixed_point --dataset quad --opt sd --dim 10 --reg-param 0.5  --ntasks 5000 --componentwise 1 --T 100 --constraint strict
python3 loss_comparison.py --dataset quad --dim 10 --reg-param 0.5  --T 100 --constraint strict 


