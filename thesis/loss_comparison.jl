using CSV
using Plots
using DataFrames
using Statistics

# Code to produce the test loss plots for Chapters 3&4. Assumes loss_comparison.py has been run to produce the necessary data to the ./results/{strict,relaxed,none}/ folder.

cd("./results/strict/")

for folder in filter(text -> occursin("quad", text), readdir())

datadict = Dict()

for file in filter(text -> prod(occursin.(["loss_comparison",".csv"], text)), readdir(folder))
    dataname = split(file, ('_','.'))[end-1]
    datadict[dataname] = convert(Matrix, CSV.read(folder*"/"*file, header=false))
end

plot_loss(keys, losses) = begin
    stds = std.(eachcol(losses))
    means = mean.(eachcol(losses))
#    medians = median.(eachcol(losses))
    plot!(1:size(losses)[2], means, label=keys)
end

plot()
datas = filter(k->k[1]!="param", datadict)
plot_loss.(keys(datas), values(datas))
plot!(xlabel="t", ylabel="Test loss (log-scale)",dpi=300, yaxis=:log)

savefig(folder*"/loss_comparison_"*string(length(datadict))*".png")
end
