#!/usr/bin/env python3
import pdb
import hypergrad as hg
import torch
from torch.nn import functional as F


def detach_with_grad(tensor):
    return tensor.detach().clone().requires_grad_(True)


def fixed_point(
    final_params,
    hx,
    hparams,
    K,
    inner_opt,
    outer_loss,
    tol=1e-10,
    set_grad=True,
    jacobian_param=None,
):
    """
    Implementation of the fixed point algorithm defined in Section 2.4.

    Parameters
    ----------
    final_params = final parameter from the optimiser iteration, theta_T
    hx = final hidden state from the optimiser iteration, h_T
    hparams = optimiser parameters
    K = maximum fixed point iterations
    inner_opt = inner optimiser, needs to have transition and output mappings defined
    outer_loss = validation loss definition, needs to have the interface outer_loss(params, hparams)
    tol = tolerance for checking for convergence of the fixed point method
    set_grad = set the final hypergradient estimate to each parameter hparams
    jacobian_param = value for the hyperparameter that controls the Jacobian column sums. if None, no Jacobian penalty is used

    Returns
    -------
    grads = final hypergradient estimate from the fixed point iteration


    """

    fp_map = inner_opt.transition
    output_map = inner_opt.output

    hx = [h.detach().requires_grad_(True) for h in hx]
    final_hx = fp_map(final_params, hx, hparams, create_graph=True)
    final_params = output_map(final_params, final_hx, hparams)
    final_duplicates = [p.detach().requires_grad_(True) for p in final_params]

    if jacobian_param is not None:
        # Jacobian of final state with respect to the second to last one, used to enforce local stability
        jacobian_grad = hg.torch_grad(
            final_hx,
            hx,
            grad_outputs=[torch.ones_like(p, requires_grad=True) for p in hx],
            allow_unused=True,
            create_graph=True,
            retain_graph=True,
        )
        jacobian_penalty = 10 * sum(
            [(j - jacobian_param).clamp(min=0).norm() for j in jacobian_grad]
        )
        # if jacobian_penalty > 0:
        #    print(f"Jacobian penalty={jacobian_penalty}, grad={jacobian_grad}")
    else:
        jacobian_penalty = 0

    o_loss = outer_loss(final_duplicates, hparams)
    grad_outer_params, grad_outer_hparams = hg.get_outer_gradients(
        o_loss + jacobian_penalty, final_duplicates, hparams
    )

    bs = hg.grad_unused_zero(
        final_params, final_hx, grad_outputs=grad_outer_params, retain_graph=True
    )

    # Iterate the upper fixed point system to calculate vector v
    vs = [torch.zeros_like(p) for p in final_params]
    vs_vec = hg.cat_list_to_tensor(vs)
    for k in range(K):
        vs_prev_vec = vs_vec

        vs = hg.torch_grad(final_hx, hx, grad_outputs=vs, retain_graph=True)
        vs = [v + b for v, b in zip(vs, bs)]
        vs_vec = hg.cat_list_to_tensor(vs)

        if float(torch.norm(vs_vec - vs_prev_vec)) < tol:
            break

    # Jacobian-vector product of outer objective with respect to lambda, times v
    ds = hg.grad_unused_zero(final_hx, hparams, grad_outputs=vs, retain_graph=True)
    final_hx2 = [h.detach().requires_grad_(True) for h in hx]
    final_params2 = output_map(final_params, final_hx2, hparams)
    cs = hg.grad_unused_zero(
        final_params2, hparams, grad_outputs=grad_outer_params, retain_graph=True
    )

    grads = [c + d + h for c, d, h in zip(cs, ds, grad_outer_hparams)]

    if set_grad:
        hg.update_tensor_grads(hparams, grads)

    return grads


def reverse(
    final_params,
    hx_history,
    hparams,
    K,
    inner_opt,
    outer_loss,
    tol=1e-10,
    set_grad=True,
):
    """
    Implementation of the K-truncated reverse-mode algorithm defined in Section 2.4.

    Parameters
    ----------
    final_params = final parameter from the optimiser iteration, theta_T
    hx_history = the history of hidden states from the optimiser iteration, h_0,...,h_T
    hparams = optimiser parameters
    K = how many steps to backpropagate before truncating
    inner_opt = inner optimiser, needs to have transition and output mappings defined
    outer_loss = validation loss definition, needs to have the interface outer_loss(params, hparams)
    tol = tolerance for checking for convergence of the fixed point method
    set_grad = set the final hypergradient estimate to each parameter hparams

    Returns
    -------
    grads = final hypergradient estimate from the K-truncated reverse-mode algorithm


    """

    fp_map = inner_opt.transition
    output_map = inner_opt.output
    hx_history = [[h.detach().requires_grad_(True) for h in hx] for hx in hx_history]
    final_params = [fp.detach().requires_grad_(True) for fp in final_params]
    final_duplicates = [fp.detach().requires_grad_(True) for fp in final_params]

    final_params = output_map(final_params, hx_history[-1], hparams)
    o_loss = outer_loss(final_duplicates, hparams)

    grad_outer_params, grad_outer_hparams = hg.get_outer_gradients(
        o_loss, final_duplicates, hparams
    )

    grad_param_hparams = hg.grad_unused_zero(
        final_params, hparams, grad_outputs=grad_outer_params, retain_graph=True
    )

    alphas = hg.torch_grad(
        final_params, hx_history[-1], grad_outputs=grad_outer_params, retain_graph=True,
    )

    grads = [torch.zeros_like(w) for w in hparams]
    K = len(hx_history) - 1
    for k in range(-2, -(K + 2), -1):
        hx_mapped = fp_map(final_params, hx_history[k], hparams)
        bs = hg.grad_unused_zero(
            hx_mapped, hparams, grad_outputs=alphas, retain_graph=True
        )
        grads = [g + b for g, b in zip(grads, bs)]
        alphas = hg.torch_grad(hx_mapped, hx_history[k], grad_outputs=alphas)

    grads = [
        p + g + v for p, g, v in zip(grad_param_hparams, grads, grad_outer_hparams)
    ]

    if set_grad:
        hg.update_tensor_grads(hparams, grads)
    return grads


def get_dim(dim, randomised=False):
    if dim > 2 and randomised:
        dim_data = (torch.randint(2, dim, [1, 1]).item(), 1)
    else:
        dim_data = (max(dim, 2), 1)
    return dim_data


def plot_quadratic_path(train, target, reg_param, params_hist, ax, soln=None):
    """
    Helper to visualise 2-dimensional quadratic optimisation.
    """
    xs = torch.linspace(-4, 4, 500)
    ys = torch.linspace(-4, 4, 500)
    grid = torch.stack(torch.meshgrid(xs, ys), dim=2)
    predictions = torch.einsum("ijk,lmk->lmj", train, grid)
    losses = F.mse_loss(
        predictions, target.squeeze().expand(500, 500, 2), reduction="none"
    ).sum(dim=2) + 0.5 * reg_param * torch.sum(grid ** 2, dim=2)
    ax.contour(grid[:, :, 0], grid[:, :, 1], losses)
    try:
        x, y = train.squeeze(0), target.squeeze(0)
        rr_soln = torch.inverse(x.T @ x + reg_param * torch.eye(2)) @ x.T @ y
        ax.plot(rr_soln[0], rr_soln[1], "*r", label="True solution")
    except:
        pass
    if soln is not None:
        ax.plot(soln[0], soln[1], "*k", label="True solution")
    params = list_to_tensor(params_hist)
    ax.plot(params[:, 0], params[:, 1], "b")
    ax.plot(params[1:, 0], params[1:, 1], ".g")
    ax.plot(params[0, 0], params[0, 1], "*g", label="Initial value")
    ax.legend()
    return


def plot_path(loss_fn, params_hist, ax, soln=None):
    """
    Helper to visualise 2-dimensional quadratic optimisation.
    """
    xs = torch.linspace(-4, 4, 500)
    ys = torch.linspace(-4, 4, 500)
    grid = torch.stack(torch.meshgrid(xs, ys), dim=2)
    losses = loss_fn(grid.unsqueeze(0))
    ax.contour(grid[:, :, 0], grid[:, :, 1], losses)
    params = list_to_tensor(params_hist)
    ax.plot(params[:, 0], params[:, 1], "b")
    ax.plot(params[1:, 0], params[1:, 1], ".g")
    ax.plot(params[0, 0], params[0, 1], "*g", label="Initial value")
    ax.legend()
    return


def list_to_tensor(list_of_tensors):
    return torch.stack([p[0] for p in list_of_tensors]).detach().squeeze()
