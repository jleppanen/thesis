#!/usr/bin/env python3

import torch
import geoopt
import pdb
from torch.nn import functional as F
from functools import reduce
from matplotlib import pyplot as plt
from geoopt import Stiefel, Euclidean, EuclideanStiefel

# Script to produce comparison of Riemannian vs. standard optimisation methods for Appendix B


dim_data = (10, 10)
torch.random.manual_seed(0)
target = torch.rand(*dim_data)
params = [torch.randn(*dim_data, requires_grad=True)]
U, S, V = torch.svd(params[0])
svd_params = [
    geoopt.Stiefel().random_naive(*dim_data).requires_grad_(True),
    geoopt.Euclidean().random(*dim_data).requires_grad_(True),
    geoopt.Stiefel().random_naive(*dim_data).requires_grad_(True),
]
svd_eye_params = [
    geoopt.ManifoldTensor(
        U.detach().clone(), manifold=EuclideanStiefel(), requires_grad=True
    ),
    geoopt.ManifoldTensor(
        torch.diag(S).detach().clone(), manifold=Euclidean(), requires_grad=True
    ),
    geoopt.ManifoldTensor(
        V.detach().clone(), manifold=EuclideanStiefel(), requires_grad=True
    ),
]

params_hist, losses = [], []
svd_params_hist, svd_losses = [], []
svd_eye_params_hist, svd_eye_losses = [], []
svd_top_opt = geoopt.optim.RiemannianAdam(svd_params)
svd_eye_top_opt = geoopt.optim.RiemannianAdam(svd_eye_params)
top_opt = torch.optim.Adam(params)

for i in range(3000):
    loss = F.mse_loss(params[0], target, reduction="sum")
    svd_param = reduce(torch.matmul, svd_params)
    svd_eye_param = reduce(torch.matmul, svd_eye_params)
    svd_loss = F.mse_loss(svd_param, target, reduction="sum")
    svd_eye_loss = F.mse_loss(svd_eye_param, target, reduction="sum")
    top_opt.zero_grad()
    svd_top_opt.zero_grad()
    loss.backward()
    svd_loss.backward()
    svd_eye_loss.backward()
    top_opt.step()
    svd_top_opt.step()
    svd_eye_top_opt.step()
    losses.append(loss.detach().data)
    svd_losses.append(svd_loss.detach().data)
    svd_eye_losses.append(svd_eye_loss.detach().data)
    params_hist.append(params[0].clone().detach().reshape(-1, 1))
    svd_params_hist.append(svd_param.clone().detach().reshape(-1, 1))
    svd_eye_params_hist.append(svd_eye_param.clone().detach().reshape(-1, 1))

f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.plot(losses, "b", label="Standard")
ax1.plot(svd_losses, "m", label="SVD random")
ax1.plot(svd_eye_losses, "g", label="SVD derived")
ax1.set_ylabel("Loss")
ax2.plot(
    abs((torch.stack(params_hist) - torch.stack(svd_params_hist))).sum(axis=1),
    "m",
    label="SVD random - Standard",
)
ax2.plot(
    abs((torch.stack(params_hist) - torch.stack(svd_eye_params_hist))).sum(axis=1),
    "g",
    label="SVD derived - Standard",
)
ax2.set_xlabel("Iterations")
ax2.set_ylabel("Sum of absolute difference")
ax1.legend()
ax2.legend()

f.savefig("./visualisations/geoopt_verification.png")
