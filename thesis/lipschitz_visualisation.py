#!/usr/bin/env python3

import argparse
import numpy as np
import torch

from torchmeta.utils.data import BatchMetaDataLoader

from optimizers import *
import os
from main2 import Task
from tqdm import tqdm

# Code to produce the output required for the Lipschitz constant estimate plots in Chapter 4.


def main():

    parser = argparse.ArgumentParser(
        description="Visualisation for Lipschitz constants"
    )
    parser.add_argument("--seed", type=int, default=5)
    parser.add_argument("--dataset", type=str, default="quad", help="dataset to use")
    parser.add_argument("--opt", type=str, default="all", help="optimiser architure")
    parser.add_argument("--dim", type=int, default=10, help="dimension")
    parser.add_argument("--T", type=int, default=100, help="number inner loop steps")
    parser.add_argument(
        "--structured",
        type=bool,
        default=False,
        help="draw data from non-standard normal",
    )
    parser.add_argument(
        "--reg-param", type=float, default=0.5, help="regularisation parameter"
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    parser.add_argument(
        "--constraint",
        type=str,
        default="strict",
        help="model constraint - strict/relaxed/none",
    )
    args = parser.parse_args()

    eval_bound = 0.98

    n_tasks_test = 1000
    reg_param = args.reg_param
    T = args.T

    loc = locals()
    del loc["parser"]
    del loc["args"]
    model_folder = (
        f"results/{args.constraint}/{args.dataset}_{args.dim}_{args.structured}_{T}"
    )
    print(args, "\n", loc, "\n")

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    if args.structured:
        cov_mat = torch.diag(torch.linspace(0.1, 0.5, args.dim))
        means = torch.linspace(0.5, 1.0, args.dim)
    else:
        cov_mat, means = None, None

    test_dataset = quadratics(
        1,
        num_tasks=n_tasks_test,
        dim=args.dim,
        covariance=cov_mat,
        means=means,
        seed=args.seed + 1,
    )
    model = torch.nn.Linear(args.dim, 1, bias=False)
    test_dataloader = BatchMetaDataLoader(test_dataset, batch_size=n_tasks_test)

    inner_opt_kwargs = {}

    if args.opt is "all":
        meta_models = []
        for file in os.listdir(model_folder):
            if file.startswith("model_"):
                meta_models.append(torch.load(os.path.join(model_folder, file)))
    else:
        meta_models = [
            torch.load(os.path.join(model_folder, f"model_{args.opt.upper()}"))
        ]

    for i, m in enumerate(meta_models):
        m.eval()

        def get_inner_opt(train_loss, **inner_opt_kwargs):
            opt = m.get_optimiser(train_loss, **inner_opt_kwargs)
            return opt

        lipschitz_estimates = []
        for k, batch in enumerate(test_dataloader):
            tr_xs, tr_ys, tr_solns = (
                batch["train"][0].to(device),
                batch["train"][1].to(device),
                batch["train"][2],
            )
            tst_xs, tst_ys, tst_solns = (
                batch["test"][0].to(device),
                batch["test"][1].to(device),
                batch["test"][2],
            )

            for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in tqdm(
                enumerate(zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns))
            ):

                model.reset_parameters()
                task = Task(reg_param, model, (tr_x, tr_y, tst_x, tst_y))

                inner_opt = get_inner_opt(task.train_loss_f, **inner_opt_kwargs)
                params = [
                    p.detach().clone().requires_grad_(True) for p in model.parameters()
                ]

                params_history, hx_history, l_history = eval_lipschitz(
                    [hp for hp in m.parameters()],
                    params,
                    inner_opt,
                    args.T,
                    log_interval=None,
                )

                lipschitz_estimates.append(l_history)

        np.savetxt(
            f"{model_folder}/lipschitz_estimates_{repr(m)}.csv",
            np.stack(lipschitz_estimates).max(axis=0),
        )


def eval_lipschitz(hparams, params, optim, n_steps, log_interval, create_graph=False):
    params_history = [params]
    hx_history = [[p.detach().clone().requires_grad_(True) for p in params]]
    l_estimates = []
    for t in range(n_steps):
        output, new_hx = optim(
            params_history[-1], hx_history[-1], hparams, create_graph=create_graph
        )
        params_history.append(output)
        hx_history.append(new_hx)
        y = optim(output, new_hx, hparams)[0]
        l_estimate = 0
        for _ in range(10):
            epsilon = torch.randn(output[0].shape[1]) / 100
            x = optim([output[0] + epsilon], [new_hx[0] + epsilon], hparams)[0]
            local_lipschitz = (x[0] - y[0]).norm() / epsilon.norm()
            l_estimate = max(local_lipschitz, l_estimate)
        l_estimates.append(l_estimate.data)

    return params_history, hx_history, l_estimates


if __name__ == "__main__":
    main()
