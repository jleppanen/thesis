using CSV
using Plots

cd("/home/juha/code/Thesis/thesis/results/final/hg_cv")
datadict = Dict()

for file in filter(text -> occursin(".csv", text), readdir())
    dataname = split(file, ('_','.'))[end-1]
    datadict[dataname] = CSV.read(file, header=false)
end
gr()
plot(datadict["lr"][!,1], datadict["hg"][!,1], label="Hypergradient", ylims=(-10,10), xlabel="Learning rate", ylabel="Hypergradient")
plot!(datadict["lr"][!,1], NaN.*datadict["lr"][!,1], label = "Loss", linecolor=:green, grid=false, legend=:top)
plot!(twinx(), datadict["lr"][!,1], datadict["losses"][!,1], linecolor=:green, ylims=(10,15), ylabel="Loss", legend=false)
savefig("/home/juha/code/Thesis/thesis/visualisations/hg_cv.png")
