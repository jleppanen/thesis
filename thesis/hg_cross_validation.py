#!/usr/bin/env python3

import argparse
import time

import numpy as np
import torch
from torchmeta.utils.data import BatchMetaDataLoader
import hypergrad as hg

from optimizers import *
from helpers import fixed_point, list_to_tensor
from matplotlib import pyplot as plt
from quadratic_experiment import Task, inner_loop, evaluate

from tqdm import tqdm

# Code to produce the output required for the cross-validated learning rate vs. hypergradient estimate comparison plot in Appendix B


def main():

    parser = argparse.ArgumentParser(
        description="Visualising different hypergradient approximation methods"
    )
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument(
        "--hg-mode",
        type=str,
        default="fixed_point",
        metavar="N",
        help="hypergradient approximation: TBPTT or fixed_point",
    )
    parser.add_argument("--opt", type=str, default="lr", help="optimiser architure")
    parser.add_argument("--dataset", type=str, default="quad", help="dataset to use")
    parser.add_argument("--dim", type=int, default=2, help="dimensionality of data")
    parser.add_argument("--T", type=int, default=100, help="number inner loop steps")
    parser.add_argument(
        "--K", type=int, default=40, help="number of hg estimation steps"
    )
    parser.add_argument(
        "--reg-param", type=float, default=0.5, help="regularisation parameter"
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    args = parser.parse_args()

    batch_size = 1
    n_tasks_test = 1
    if args.dataset == "quad":
        reg_param = args.reg_param
        T, K = args.T, args.K
    else:
        raise NotImplementedError(args.dataset, " not implemented!")

    T_test = T

    loc = locals()
    del loc["parser"]
    del loc["args"]

    print(args, "\n", loc, "\n")

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    dim = args.dim
    dataset = quadratics(1, num_tasks=n_tasks_test, dim=dim, seed=args.seed)
    model = torch.nn.Linear(dim, 1, bias=False)

    if args.opt == "lr":
        meta_model = LRMeta(dim, dim)
        init_params = torch.tensor(0.01)
    elif args.opt == "rnn":
        meta_model = RNNMeta(dim, dim)
        # init_params = [p.data.clone() for p in meta_model.parameters()]
    elif args.opt == "compwise":
        meta_model = CompwiseMeta(dim, dim)

    dataloader = BatchMetaDataLoader(dataset, batch_size=batch_size, **kwargs)

    inner_opt_kwargs = {}

    def get_inner_opt(train_loss):
        opt = meta_model.get_optimiser(train_loss, **inner_opt_kwargs)
        return opt

    tr, ta, soln = [None] * 3

    for k, batch in enumerate(dataloader):
        start_time = time.time()
        meta_model.train()

        tr_xs, tr_ys, tr_solns = (
            batch["train"][0].to(device),
            batch["train"][1].to(device),
            batch["train"][2],
        )
        tst_xs, tst_ys, tst_solns = (
            batch["test"][0].to(device),
            batch["test"][1].to(device),
            batch["test"][2],
        )
        hg_vals, test_losses = [], []
        iter_grid = torch.linspace(0.0, 0.08, 100)
        for lr in tqdm(iter_grid):
            for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in enumerate(
                zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns)
            ):
                torch.nn.init.constant_(meta_model.a, lr)
                start_time_task = time.time()

                if tr is None:
                    tr, ta, soln = tr_x, tr_y, tr_soln
                tr_x, tr_y, tr_soln = tr, ta, soln

                task = Task(
                    reg_param,
                    model,
                    (tr_x, tr_y, tst_x, tst_y),
                    batch_size=tr_xs.shape[0],
                )

                inner_opt = get_inner_opt(task.train_loss_f)
                params_history = inner_loop(
                    [hp for hp in meta_model.parameters()],
                    [p for p in model.parameters()],
                    inner_opt,
                    T,
                    log_interval=None,
                )
                last_param = params_history[-1]
                forward_time_task = time.time() - start_time_task

                if args.hg_mode == "fixed_point":
                    hg_val = fixed_point(
                        last_param,
                        [hp for hp in meta_model.parameters()],
                        K=K,
                        fp_map=inner_opt,
                        outer_loss=task.val_loss_f,
                    )
                elif args.hg_mode == "reverse":
                    hg_val = hg.reverse(
                        params_history[-fp_iter - 1 :],
                        [hp for hp in meta_model.parameters()],
                        [inner_opt] * fp_iter,
                        outer_loss=task.val_loss_f,
                    )
                else:
                    raise NotImplementedError
            hg_vals.append(hg_val)

            test_loss = evaluate(
                n_tasks_test,
                dataloader,
                model,
                meta_model,
                T_test,
                get_inner_opt,
                reg_param,
                tr,
                ta,
                log_interval=None,
            )
            test_losses.append(test_loss)
    hg_tensor = list_to_tensor(hg_vals)
    losses_tensor = torch.tensor(test_losses)
    num_parameters = len(
        [p.numel() for p in meta_model.parameters() if p.requires_grad]
    )
    f, ax = plt.subplots(1)
    ax.plot(iter_grid, hg_tensor)
    ax.plot(iter_grid, losses_tensor)
    ax.set_xlabel("Learning rate")
    ax.set_ylabel("Hypergradient")
    ax.set_ylim(-20, 20)
    f.savefig(f"visualisations/{args.opt}_hg_CV.png")
    experiment = {
        "train": tr.squeeze(),
        "target": ta.squeeze(),
        "hg": hg_tensor,
        "cv_losses": losses_tensor,
        "lr": iter_grid,
    }
    torch.save(experiment, f"results/hg_CV_opt_{args.opt}_dim_{dim}")
    for k, v in experiment.items():
        np.savetxt(
            f"results/hg_CV_opt_{args.opt}_dim_{dim}_values_{k}.csv", v.numpy(),
        )


if __name__ == "__main__":
    main()
