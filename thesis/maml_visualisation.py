#!/usr/bin/env python3

import argparse

import numpy as np
import torch
from torch.nn import functional as F

from torchmeta.toy.helpers import sinusoid
from torchmeta.toy.sinusoid import Sinusoid
from torchmeta.utils.data import BatchMetaDataLoader

import pdb
import os

from maml import evaluate, SDOpt, CRNNOpt, Task, plot_path, inner_loop

# Code to produce the output required for the test loss tables in the few-shot adaptation experiment


def main():

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument(
        "--dataset",
        type=str,
        default="sin",
        metavar="N",
        help="omniglot or miniimagenet",
    )
    parser.add_argument(
        "--opt",
        type=str,
        default="sd",
        metavar="N",
        help="optimiser architecture sd or crnn",
    )
    parser.add_argument(
        "--shots", type=int, default=10, metavar="N", help="which data to test",
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    parser.add_argument(
        "--constraint",
        type=str,
        default="strict",
        help="model constraint - strict/relaxed",
    )

    parser.add_argument(
        "--L",
        type=int,
        default=10,
        help="Lipschitz smoothness assumption for training loss",
    )

    args = parser.parse_args()

    log_interval = 100
    eval_interval = 100  # 500
    inner_log_interval = None
    inner_log_interval_test = None
    ways = 5
    batch_size = 16
    n_tasks_test = 1000
    if args.dataset == "omniglot":
        reg_param = 2  # reg_param = 2
        T, K = 16, 5  # T, K = 16, 5
    elif args.dataset == "miniimagenet":
        reg_param = 0.5  # reg_param = 0.5
        T, K = 10, 5  # T, K = 10, 5
    elif args.dataset == "sin":
        reg_param = 0.5
        T, K = 1, 40  # T, K = 10, 5
    else:
        raise NotImplementedError(args.dataset, " not implemented!")

    T_test = T

    loc = locals()
    del loc["parser"]
    del loc["args"]

    print(args, "\n", loc, "\n")

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    if args.dataset == "sin":
        transform = lambda array: torch.from_numpy(array).float()
        plots_dataset = sinusoid(
            shots=args.shots,
            test_shots=args.shots,
            seed=args.seed,
            transform=transform,
            target_transform=transform,
        )
        test_dataset = sinusoid(
            shots=args.shots,
            test_shots=args.shots,
            seed=args.seed,
            transform=transform,
            target_transform=transform,
        )
    else:
        raise NotImplementedError(
            "DATASET NOT IMPLEMENTED! only omniglot and miniimagenet "
        )

    test_dataloader = BatchMetaDataLoader(test_dataset, batch_size=batch_size, **kwargs)
    plots_dataloader = BatchMetaDataLoader(
        plots_dataset, batch_size=6, shuffle=False, **kwargs
    )

    inner_opt_kwargs = {"L": args.L, "compwise": True, "constraint": args.constraint}

    def get_inner_opt(train_loss):
        opt = inner_opt_class(train_loss, **inner_opt_kwargs)
        return opt

    model_folder = f"results/maml/{args.constraint}_{args.L}/"
    inner_opts = [SDOpt, CRNNOpt]

    inner_opt_params = []
    meta_models = []
    for opt in inner_opts:
        opt_name = opt.__name__.replace("Opt", "").lower()
        for file in os.listdir(model_folder):
            if file.startswith(f"meta_model_shots_{args.shots}") and file.endswith(
                opt_name
            ):
                meta_models.append(torch.load(os.path.join(model_folder, file)))
            if file.startswith(f"opt_params_shots_{args.shots}") and file.endswith(
                opt_name
            ):
                inner_opt_params.append(torch.load(os.path.join(model_folder, file)))

    # Calculate total test losses
    for inner_opt, inner_opt_param, meta_model in zip(
        inner_opts, inner_opt_params, meta_models
    ):

        inner_opt_class = inner_opt
        test_losses, test_accs = evaluate(
            n_tasks_test,
            test_dataloader,
            meta_model,
            T_test,
            get_inner_opt,
            inner_opt_param,
            reg_param,
        )
        print(
            f"Test loss for {inner_opt_class.__name__}: {round(test_losses.mean(),3)} +- {round(test_losses.std(),3)}"
        )

    # Produce data for sample plots
    grid = torch.linspace(-5, 5, 1000)
    for _, batch in enumerate(plots_dataloader):
        tr_xs, tr_ys = batch["train"][0].to(device), batch["train"][1].to(device)
        tst_xs, tst_ys = batch["test"][0].to(device), batch["test"][1].to(device)

        for t_idx, (tr_x, tr_y, tst_x, tst_y) in enumerate(
            zip(tr_xs, tr_ys, tst_xs, tst_ys)
        ):
            plot_data = [grid]
            task = Task(reg_param, meta_model, (tr_x, tr_y, tst_x, tst_y))
            for inner_opt_cls, inner_opt_param, meta_model in zip(
                inner_opts, inner_opt_params, meta_models
            ):
                inner_opt_class = inner_opt_cls
                inner_opt = get_inner_opt(task.train_loss_f)

                params = [
                    p.detach().clone().requires_grad_(True)
                    for p in meta_model.parameters()
                ]

                params_history = inner_loop(
                    inner_opt_param, params, inner_opt, 1, log_interval=None,
                )
                for p in params_history:
                    plot_data.append(
                        task.fmodel(grid.view(-1, 1, 1), params=p).squeeze().detach()
                    )

                np.savetxt(
                    f"results/maml/plot_data/plot_data_{t_idx}.csv",
                    np.stack(plot_data),
                )
                np.savetxt(
                    f"results/maml/plot_data/inputs_data_{t_idx}.csv",
                    torch.stack([tr_x, tr_y]).squeeze().numpy(),
                )
        np.savetxt(
            f"results/maml/plot_data/amplitudes.csv", plots_dataset.amplitudes[:6],
        )
        np.savetxt(
            f"results/maml/plot_data/phases.csv", plots_dataset.phases[:6],
        )
        break


if __name__ == "__main__":
    main()
