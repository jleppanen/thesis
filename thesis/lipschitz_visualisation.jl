using CSV
using Plots
using LaTeXStrings

# Code to produce the Lipschitz constant estimate plots in Chapter 3.
# Assumes lipschitz_visualisation.py has been run such that the necessary output folders ./results/{strict,relaxed,none}/quad/ contain the data required for the plots.

cd("./results/strict/")

for folder in filter(text -> occursin("quad", text), readdir())

datadict = Dict()

for file in filter(text -> prod(occursin.(["lipschitz",".csv"], text)), readdir(folder))
    dataname = split(file, ('_','.'))[end-1]
    datadict[dataname] = convert(Matrix, CSV.read(folder*"/"*file, header=false))
end

plot_loss(keys, lipschitz) = begin
    plot!(1:size(lipschitz)[1], lipschitz, label=keys, linetype=:scatter)
end

    plot([1.0], linetype=:hline, label="", linestyle=:dash, linecolor=:red,xlabel="t", ylabel=L"\hat{L}_{\Phi}",dpi=300, legend=:topright)
    ylims!(0.85,1.10)
datas = filter(k->k[1]!="param", datadict)
plot_loss.(keys(datas), values(datas))

savefig(folder*"/lipschitz_estimates_"*string(length(datadict))*".png")
end
