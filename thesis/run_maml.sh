#!/usr/bin/env bash

mkdir -p results/maml/strict_10
python3 maml.py --opt crnn --L 10 --constraint strict --shots 10
python3 maml.py --opt crnn --L 10 --constraint strict --shots 5
python3 maml.py --opt sd --L 10 --shots 10
python3 maml.py --opt sd --L 10 --shots 5

mkdir -p results/maml/strict_20
python3 maml.py --opt crnn --L 20 --constraint strict --shots 10
python3 maml.py --opt crnn --L 20 --constraint strict --shots 5
python3 maml.py --opt sd --L 20 --shots 10
python3 maml.py --opt sd --L 20 --shots 5

mkdir -p results/maml/strict_50
python3 maml.py --opt crnn --L 50 --constraint strict --shots 10
python3 maml.py --opt crnn --L 50 --constraint strict --shots 5
python3 maml.py --opt sd --L 50 --shots 10
python3 maml.py --opt sd --L 50 --shots 5
