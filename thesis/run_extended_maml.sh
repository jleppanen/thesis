#!/usr/bin/env bash
mkdir -p results/maml/relaxed_10
python3 maml_experiment.py --opt crnn --L 10 --constraint relaxed --shots 10
python3 maml_experiment.py --opt crnn --L 10 --constraint relaxed --shots 5

mkdir -p results/maml/relaxed_20
python3 maml_experiment.py --opt crnn --L 20 --constraint relaxed --shots 10
python3 maml_experiment.py --opt crnn --L 20 --constraint relaxed --shots 5

mkdir -p results/maml/relaxed_50
python3 maml_experiment.py --opt crnn --L 50 --constraint relaxed --shots 10
python3 maml_experiment.py --opt crnn --L 50 --constraint relaxed --shots 5
