#!/usr/bin/env python3

import argparse
import time

import numpy as np
import torch
from torch.nn import functional as F

from torchmeta.toy.helpers import sinusoid
from torchmeta.utils.data import BatchMetaDataLoader

import higher

import pdb
from matplotlib import pyplot as plt
import os

import hypergrad as hg

# The main file for training optimisers in the few-shot model adaptation experiment. See run_maml.sh for example shell commands for training.


class Task:
    """
    Task definition, using MSE loss for both training and validation but with different data
    """

    def __init__(self, reg_param, meta_model, data, batch_size=None):
        device = next(meta_model.parameters()).device

        # stateless version of meta_model
        self.fmodel = higher.monkeypatch(
            meta_model, device=device, copy_initial_weights=True
        )

        self.n_params = len(list(meta_model.parameters()))
        self.train_input, self.train_target, self.test_input, self.test_target = data
        self.reg_param = reg_param
        self.batch_size = 1 if not batch_size else batch_size
        self.val_loss, self.val_acc = None, None

    def train_loss_f(self, params, hparams=None):
        out = self.fmodel(self.train_input, params=params)
        return F.mse_loss(out, self.train_target)

    def val_loss_f(self, params, hparams):
        out = self.fmodel(self.test_input, params=params)
        val_loss = F.mse_loss(out, self.test_target) / self.batch_size
        self.val_loss = val_loss.item()
        return val_loss


class SDOpt(hg.DifferentiableOptimizer):
    """
    Reimplementation of the SD-c optimiser as a Hypertorch DifferentiableOptimizer.
    """

    def __init__(self, loss_f, L, compwise, data_or_iter=None, **kwargs):
        super(SDOpt, self).__init__(loss_f, dim_mult=1, data_or_iter=data_or_iter)
        self.L = L
        self.compwise = compwise

    def apply_constraints(self, opt_params):
        (step_size, *_) = opt_params
        return [step_size.clamp(min=0.0001, max=2.0 / self.L)]

    def transition(self, params, hidden, opt_params, create_graph=True):
        loss = self.get_loss(params, opt_params)
        (step_size,) = self.apply_constraints(opt_params)
        grads = torch.autograd.grad(loss, params, create_graph=create_graph)
        return [w - step_size * g for w, g in zip(params, grads)]

    def output(self, params, hidden, opt_params):
        return hidden

    def step(self, params, hparams, create_graph):
        new_hidden = self.transition(params, params, hparams, create_graph=True)
        return self.output(params, new_hidden, hparams)

    def __repr__(self):
        return self.__class__.__name__.replace("Opt", "")


class CRNNOpt(hg.DifferentiableOptimizer):
    """
    Reimplementation of the CRNN-c optimiser as a Hypertorch DifferentiableOptimizer.
    """

    def __init__(self, loss_f, L, compwise, constraint, data_or_iter=None):
        super(CRNNOpt, self).__init__(loss_f, dim_mult=1, data_or_iter=data_or_iter)
        self.L = L
        self.compwise = compwise
        self.constraint = constraint

    def apply_constraints(self, opt_params):
        (w, u, *_) = opt_params
        w_constrained = w.clamp(min=0.0, max=0.98)
        if self.constraint == "strict":
            u_max = (1 - w_constrained.data) / self.L
        elif self.constraint == "relaxed":
            u_max = 2 / self.L
        else:
            NotImplementedError

        u_constrained = u.clamp(min=-u_max, max=u_max)
        return w_constrained, u_constrained

    def transition(self, params, hidden, opt_params, create_graph=True):
        loss = self.get_loss(params, opt_params)
        grads = torch.autograd.grad(loss, params, create_graph=create_graph)
        w, u = self.apply_constraints(opt_params)
        return [w * p + u * g for p, g in zip(hidden, grads)]

    def output(self, params, hidden, opt_params):
        return hidden

    def step(self, params, hparams, create_graph):
        new_hidden = self.transition(params, params, hparams, create_graph=True)
        return self.output(params, new_hidden, hparams)

    def __repr__(self):
        return f"{self.__class__.__name__.replace('Opt', '')} ({self.constraint})"


def main():

    parser = argparse.ArgumentParser(description="MAML with learned optimiser")
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument(
        "--dataset",
        type=str,
        default="sin",
        metavar="N",
        help="only sinusoidal data supported",
    )
    parser.add_argument(
        "--hg-mode",
        type=str,
        default="unroll",
        metavar="N",
        help="hypergradient approximation",
    )
    parser.add_argument(
        "--opt",
        type=str,
        default="sd",
        metavar="N",
        help="optimiser architecture sd or crnn",
    )
    parser.add_argument(
        "--shots", type=int, default=10, metavar="N", help="which data to test",
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    parser.add_argument(
        "--constraint",
        type=str,
        default="strict",
        help="model constraint - strict/relaxed",
    )

    parser.add_argument(
        "--L",
        type=int,
        default=10,
        help="Lipschitz smoothness assumption for training loss",
    )

    args = parser.parse_args()

    log_interval = 100
    eval_interval = 100
    inner_log_interval = None
    inner_log_interval_test = None
    ways = 5
    batch_size = 16
    n_tasks_test = 1000
    if args.dataset == "sin":
        reg_param = 0.5  # return eg_param = 0.5
        T, K = 1, 40  #
    else:
        raise NotImplementedError(args.dataset, " not implemented!")

    T_test = T

    loc = locals()
    del loc["parser"]
    del loc["args"]

    print(args, "\n", loc, "\n")

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    if args.dataset == "sin":
        transform = lambda array: torch.from_numpy(array).float()
        n_shots = args.shots
        dataset = sinusoid(
            shots=n_shots,
            test_shots=n_shots,
            seed=args.seed,
            transform=transform,
            target_transform=transform,
        )
        test_dataset = sinusoid(
            shots=n_shots,
            test_shots=n_shots,
            seed=args.seed,
            transform=transform,
            target_transform=transform,
        )

        # Learner model definition
        meta_model = torch.nn.Sequential(
            torch.nn.Linear(1, 40),
            torch.nn.ReLU(),
            torch.nn.Linear(40, 40),
            torch.nn.ReLU(),
            torch.nn.Linear(40, 1),
        )
    else:
        NotImplementedError(args.dataset, " not implemented!")

    dataloader = BatchMetaDataLoader(dataset, batch_size=batch_size, **kwargs)
    test_dataloader = BatchMetaDataLoader(test_dataset, batch_size=batch_size, **kwargs)

    if args.opt == "sd":
        inner_opt_kwargs = {"L": args.L, "compwise": True}
        inner_opt_class = SDOpt
        inner_opt_params = [torch.nn.Parameter(torch.tensor(0.001, requires_grad=True))]
    elif args.opt == "crnn":
        inner_opt_kwargs = {
            "L": args.L,
            "compwise": True,
            "constraint": args.constraint,
        }
        inner_opt_class = CRNNOpt
        inner_opt_params = [
            torch.nn.Parameter(torch.tensor(v, requires_grad=True))
            for v in [0.88, -(1 - 0.88) / 10.0]
        ]
    else:
        raise NotImplementedError

    outer_opt = torch.optim.Adam(
        params=[
            {"params": meta_model.parameters()},
            {"params": (p for p in inner_opt_params)},
        ]
    )

    def get_inner_opt(train_loss):
        opt = inner_opt_class(train_loss, **inner_opt_kwargs)
        return opt

    for k, batch in enumerate(dataloader):
        start_time = time.time()
        meta_model.train()

        tr_xs, tr_ys = batch["train"][0].to(device), batch["train"][1].to(device)
        tst_xs, tst_ys = batch["test"][0].to(device), batch["test"][1].to(device)

        outer_opt.zero_grad()

        val_loss, val_acc = 0, 0
        forward_time, backward_time = 0, 0
        for t_idx, (tr_x, tr_y, tst_x, tst_y) in enumerate(
            zip(tr_xs, tr_ys, tst_xs, tst_ys)
        ):
            start_time_task = time.time()

            # single task set up
            task = Task(
                reg_param,
                meta_model,
                (tr_x, tr_y, tst_x, tst_y),
                batch_size=tr_xs.shape[0],
            )
            inner_opt = get_inner_opt(task.train_loss_f)

            # single task inner loop
            params = [p for p in meta_model.parameters()]
            last_param = inner_loop(
                inner_opt_params, params, inner_opt, T, log_interval=inner_log_interval,
            )[-1]
            forward_time_task = time.time() - start_time_task
            # single task hypergradient computation
            if args.hg_mode == "unroll":
                loss = task.val_loss_f(last_param, [])
                loss.backward()
            else:
                NotImplementedError

            backward_time_task = time.time() - start_time_task - forward_time_task

            val_loss += task.val_loss

            forward_time += forward_time_task
            backward_time += backward_time_task

        outer_opt.step()
        step_time = time.time() - start_time

        if k % log_interval == 0:
            print(
                "MT k={} ({:.3f}s F: {:.3f}s, B: {:.3f}s) Val Loss: {:.2e}.".format(
                    k, step_time, forward_time, backward_time, val_loss
                )
            )

        if k % eval_interval == 0:
            test_losses, test_accs = evaluate(
                n_tasks_test,
                test_dataloader,
                meta_model,
                T_test,
                get_inner_opt,
                inner_opt_params,
                reg_param,
                log_interval=inner_log_interval_test,
            )

            print(
                "Test loss {:.2e} +- {:.2e}: (mean +- std over {} tasks).".format(
                    test_losses.mean(), test_losses.std(), len(test_losses),
                )
            )

        if k % 10000 == 0:
            out_folder = f"results/maml/{args.constraint}_{args.L}"
            if not os.path.exists(out_folder):
                os.makedirs(out_folder)
            torch.save(
                meta_model, f"{out_folder}/meta_model_shots_{n_shots}_{args.opt}"
            )
            torch.save(
                list(inner_opt.apply_constraints(inner_opt_params)),
                f"{out_folder}/opt_params_shots_{n_shots}_{args.opt}",
            )
            with open(
                os.path.join(out_folder, f"train_setup_shots_{n_shots}_{args.opt}.txt"),
                "w",
            ) as out_file:
                out_file.writelines([str(args), str(loc)])

        if k == 62500:
            return


def inner_loop(hparams, params, optim, n_steps, log_interval, create_graph=False):
    params_history = [optim.get_opt_params(params)]
    for t in range(n_steps):
        params_history.append(
            optim(params_history[-1], hparams, create_graph=create_graph)
        )

        if log_interval and (t % log_interval == 0 or t == n_steps - 1):
            print("t={}, Loss: {:.6f}".format(t, optim.curr_loss.item()))

    return params_history


def evaluate(
    n_tasks,
    dataloader,
    meta_model,
    n_steps,
    get_inner_opt,
    inner_opt_params,
    reg_param,
    log_interval=None,
):
    meta_model.train()
    device = next(meta_model.parameters()).device

    val_losses, val_accs = [], []
    for k, batch in enumerate(dataloader):
        tr_xs, tr_ys = batch["train"][0].to(device), batch["train"][1].to(device)
        tst_xs, tst_ys = batch["test"][0].to(device), batch["test"][1].to(device)

        for t_idx, (tr_x, tr_y, tst_x, tst_y) in enumerate(
            zip(tr_xs, tr_ys, tst_xs, tst_ys)
        ):
            task = Task(reg_param, meta_model, (tr_x, tr_y, tst_x, tst_y))
            inner_opt = get_inner_opt(task.train_loss_f)

            params = [
                p.detach().clone().requires_grad_(True) for p in meta_model.parameters()
            ]

            params_history = inner_loop(
                inner_opt_params, params, inner_opt, n_steps, log_interval=log_interval,
            )

            # if t_idx == 0 and k == 0:
            #     plot_path(task, params_history, inner_opt_params)

            last_param = params_history[-1]
            task.val_loss_f(last_param, meta_model.parameters())

            val_losses.append(task.val_loss)
            val_accs.append(task.val_acc)

            if len(val_accs) >= n_tasks:
                return np.array(val_losses), np.array(val_accs)


def plot_path(task, params_history, inner_opt_params, amplitude=None, phase=None):
    grid = torch.linspace(-5, 5, 100)
    plt.plot(
        grid,
        task.fmodel(grid.view(-1, 1, 1), params=params_history[0]).squeeze().detach(),
        label="First",
    )
    plt.plot(
        grid,
        task.fmodel(grid.view(-1, 1, 1), params=params_history[-1]).squeeze().detach(),
        label="Last",
    )

    plt.plot(task.test_input, task.test_target, ".k", label="Test data")
    plt.legend()
    plt.title(f"Opt params: {[p.data for p in inner_opt_params]}")
    plt.ylim(-5, 5)
    if amplitude is not None and phase is not None:
        plt.plot(grid, amplitude * torch.sin(grid.squeeze() - phase), "r")
    plt.savefig("test_paths/test_maml.png")
    plt.close()
