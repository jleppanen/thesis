using CSV
using Plots
using DataFrames

# Produces the plots of hypergradient estimate vs. K in Appendix B.
# Assumes the current working directory contains the folders ["fp_convergence", "fp_divergence", "reverse_convergence"]
# that contain the output from fixed_point_visualisation.py

for folder in ["fp_convergence", "fp_divergence", "reverse_convergence"]
#cd("/home/juha/code/Thesis/thesis/results/final/"*folder)
datadict = Dict()

for file in filter(text -> occursin(".csv", text), readdir())
    dataname = split(file, ('_','.'))[end-1]
    datadict[dataname] = convert(Matrix, CSV.read(file, header=false))
end
gr()
p1 = plot(datadict["k"], datadict["W"], label="Approximate", xlabel="K", ylabel="Hypergradient of W", color=repeat([:red], 10), legend=false)
p1 = plot!(datadict["hg"][1,:], linetype=:hline, linestyle=:dot, label="Fully unrolled", xlabel="K", color=repeat([:red], 10), legend=false)
p2 = plot(datadict["k"], datadict["U"], label="U", xlabel="K", ylabel="Hypergradient of U",color=repeat([:blue], 10), legend=false)
p2 = plot!(datadict["hg"][2,:], linetype=:hline, linestyle=:dot, label="Fully unrolled", xlabel="K", color=repeat([:blue], 10), legend=false)
plot(p1,p2, layout=(1,2))
savefig("/home/juha/code/Thesis/thesis/visualisations/hg_"*folder*".png")
end
