#!/usr/bin/env python3
import numpy as np
import torch
from torchmeta.utils.data import Task, MetaDataset
from torchmeta.transforms import ClassSplitter
import pdb
import geoopt


# Contains the definitions of the CRNN, CRNN-c, SD, SD-c optimisers. Also contains the definition of the task producing random quadratics.


class CRNNMeta(torch.nn.Module):
    def __init__(
        self,
        input_size: int,
        hidden_size: int,
        L=10,
        eval_bound=0.98,
        compwise=False,
        constraint="strict",
        **kwargs,
    ) -> None:
        super(CRNNMeta, self).__init__()
        self.L = L
        self.eval_bound = eval_bound
        self.opt_kwargs = kwargs
        self.compwise = compwise
        self.constraint = constraint
        if compwise:
            self.w = torch.nn.Parameter(torch.tensor(self.eval_bound - 0.1))
            self.u = torch.nn.Parameter(torch.tensor(-0.01))
        else:
            self.Uh, self.Sh, self.Vh = self.__init_SVD(hidden_size, eval_bound - 0.1)
            self.Ui, self.Si, self.Vi = self.__init_SVD(hidden_size, -1 / 100.0)
            # self.A = torch.nn.Parameter(torch.eye(hidden_size))

    def __init_SVD(self, hidden_size, init_val):
        U = geoopt.ManifoldParameter(torch.eye(hidden_size), geoopt.Stiefel())
        S = geoopt.ManifoldParameter(
            geoopt.ManifoldTensor(
                init_val * torch.ones(hidden_size, 1), requires_grad=True
            )
        )
        V = geoopt.ManifoldParameter(torch.eye(hidden_size), geoopt.Stiefel())

        return U, S, V

    def get_optimiser(self, loss_fn, L=None, **kwargs):
        if self.training and L is not None:
            self.L = L
        if self.compwise:
            return CRNNCompwiseOpt(
                loss_fn, self.L, self.eval_bound, self.constraint, **self.opt_kwargs
            )
        else:
            return CRNNOpt(
                loss_fn, self.L, self.eval_bound, self.constraint, **self.opt_kwargs
            )

    def forward(self):
        pass

    def __repr__(self):
        return self.__class__.__name__.replace("Meta", "") + "-c" * self.compwise


class CRNNOpt(torch.nn.Module):
    """
    Componentwise optimiser with component specific hidden states.
    """

    def __init__(self, inner_loss, L, eval_bound, constraint, **kwargs):
        super(CRNNOpt, self).__init__()
        self.loss_fn = inner_loss
        self.activation = lambda x: x
        self.L = L
        self.eval_bound = eval_bound
        self.constraint = constraint

    def __RNN(self, grads, params, hparams):
        """
        Definition of the RNN update.
        """
        (Uh, Sh, Vh, Ui, Si, Vi) = hparams
        if self.constraint == "none":
            w_max = 1e8
            u_max = 1e8
        elif self.constraint == "relaxed":
            w_max = self.eval_bound
            u_max = 2 / self.L
        elif self.constraint == "strict":
            w_max = self.eval_bound
            s_max = Sh.max()
            u_max = (1 - min(s_max.data, w_max)) / self.L
        else:
            NotImplementedError
        update = [
            Uh @ torch.diag(Sh.clamp(min=0, max=w_max).flatten()) @ Vh @ h.T
            + Ui @ torch.diag(Si.clamp(min=-u_max, max=u_max).flatten()) @ Vi @ g.T
            for h, g in zip(params, grads)
        ]
        return update

    def transition(self, params, hx, hparams, create_graph=True):
        """
        Definition of the fixed point map implied by the RNN relation.
        """
        loss = self.loss_fn(hx, hparams)
        grads = torch.autograd.grad(loss, hx, create_graph=create_graph)
        updates = self.__RNN(grads, hx, hparams)
        out = [self.activation(u.T) for u in updates]
        return out

    def output(self, params, hx, hparams):
        return hx

    def forward(self, params, hx, hparams, create_graph=True):
        new_hidden = self.transition(params, hx, hparams)
        output = self.output(params, new_hidden, hparams)
        return output, new_hidden


class CRNNCompwiseOpt(torch.nn.Module):
    """
    Componentwise optimiser with component specific hidden states.
    """

    def __init__(self, inner_loss, L, eval_bound, constraint, **kwargs):
        super(CRNNCompwiseOpt, self).__init__()
        self.loss_fn = inner_loss
        self.activation = lambda x: x
        self.L = L
        self.eval_bound = eval_bound
        self.constraint = constraint

    def __RNN(self, grads, params, hparams):
        """
        Definition of the RNN update.
        """
        (w, u) = hparams
        if self.constraint == "none":
            w_max = 1e8
            u_max = 1e8
        elif self.constraint == "relaxed":
            w_max = self.eval_bound
            u_max = 2 / self.L
        elif self.constraint == "strict":
            w_max = self.eval_bound
            u_max = (1 - min(w.data, w_max)) / self.L
        else:
            NotImplementedError
        update = [
            w.clamp(min=0.0, max=w_max) * h + u.clamp(min=-u_max, max=u_max) * g
            for h, g in zip(params, grads)
        ]

        return update

    def transition(self, params, hx, hparams, create_graph=True):
        """
        Definition of the fixed point map implied by the RNN relation.
        """
        loss = self.loss_fn(hx, hparams)
        grads = torch.autograd.grad(loss, hx, create_graph=create_graph)
        updates = self.__RNN(grads, hx, hparams)
        out = [self.activation(u) for u in updates]
        return out

    def output(self, params, hx, hparams):
        return hx

    def forward(self, params, hx, hparams, create_graph=True):
        new_hidden = self.transition(params, hx, hparams)
        output = self.output(params, new_hidden, hparams)
        return output, new_hidden


class SDMeta(torch.nn.Module):
    def __init__(
        self, input_size: int, hidden_size: int, compwise=False, L=10, **kwargs
    ) -> None:
        super(SDMeta, self).__init__()
        self.loss_fn = None
        self.compwise = compwise
        self.a = torch.nn.Parameter(
            1 / (100 * L) * torch.ones(1 if compwise else input_size)
        )
        self.L = L
        self.opt_kwargs = kwargs

    def get_optimiser(self, loss_fn, L=None, **kwargs):
        if self.training and L is not None:
            self.L = L
        return SDOpt(loss_fn, L=self.L, **self.opt_kwargs)

    def forward(self):
        pass

    def __repr__(self):
        return self.__class__.__name__.replace("Meta", "") + "-c" * self.compwise


class SDOpt(torch.nn.Module):
    """
    Componentwise optimiser with component specific hidden states.
    """

    def __init__(self, inner_loss, **kwargs):
        super(SDOpt, self).__init__()
        self.loss_fn = inner_loss
        self.L = kwargs["L"]

    def transition(self, params, hx, hparams, create_graph=False):
        """
        Definition of the fixed point map implied by the RNN relation.
        """
        (a, *_) = hparams
        loss = self.loss_fn(hx, hparams)
        grads = torch.autograd.grad(loss, hx, create_graph=create_graph)
        out = [
            p - a.clamp(min=0.0001, max=2.0 / (self.L)) * g for p, g in zip(hx, grads)
        ]
        return out

    def output(self, params, hx, hparams):
        return hx

    def forward(self, params, hx, hparams, create_graph=False):
        new_hidden = self.transition(params, hx, hparams, create_graph=create_graph)
        output = self.output(params, new_hidden, hparams)
        return output, new_hidden


class Quadratics(MetaDataset):
    """
    ----------
    num_samples_per_task : int
        Number of examples per task.
    num_tasks : int (default: 1,000,000)
        Overall number of tasks to sample.
    noise_std : float, optional
        Amount of noise to include in the targets for each task. If `None`, then
        nos noise is included, and the target is either a sine function, or a
        linear function of the input.
    transform : callable, optional
        A function/transform that takes a numpy array of size (1,) and returns a
        transformed version of the input.
    target_transform : callable, optional
        A function/transform that takes a numpy array of size (1,) and returns a
        transformed version of the target.
    dataset_transform : callable, optional
        A function/transform that takes a dataset (ie. a task), and returns a
        transformed version of it. E.g. `torchmeta.transforms.ClassSplitter()`.
    Notes
    -----

    References
    ----------

    """

    def __init__(
        self,
        num_samples_per_task,
        num_tasks=1000000,
        noise_std=None,
        transform=None,
        target_transform=None,
        dataset_transform=None,
        dim=2,
        covariance=None,
        means=None,
    ):
        super(Quadratics, self).__init__(
            meta_split="train",
            target_transform=target_transform,
            dataset_transform=dataset_transform,
        )
        self.num_samples_per_task = num_samples_per_task
        self.num_tasks = num_tasks
        self.noise_std = noise_std
        self.transform = transform
        self.w_dim = dim
        self._input_range = np.array([-1.0, 1.0])
        self._w_range = np.array([-1.0, 1.0])
        self.covariance = covariance
        self.means = means
        self._ws = None

    @property
    def ws(self):
        if self._ws is None:
            self._ws = self.np_random.uniform(
                *self._w_range, size=(self.num_tasks, self.w_dim)
            )
            # self._ws = np.zeros((self.num_tasks, self.w_dim))

        return self._ws

    def __len__(self):
        return self.num_tasks

    def __getitem__(self, index):
        w = self.ws[index]

        task = QuadraticTask(
            index,
            w,
            self._input_range,
            self.noise_std,
            self.num_samples_per_task,
            self.transform,
            self.target_transform,
            np_random=self.np_random,
            covariance=self.covariance,
            means=self.means,
        )

        if self.dataset_transform is not None:
            task = self.dataset_transform(task)

        return task


class QuadraticTask(Task):
    def __init__(
        self,
        index,
        w,
        input_range,
        noise_std,
        num_samples,
        transform=None,
        target_transform=None,
        np_random=None,
        covariance=None,
        means=None,
    ):
        super(QuadraticTask, self).__init__(index, None)  # Regression task
        self.w = w
        self.input_range = input_range
        self.num_samples = num_samples
        self.noise_std = noise_std
        self.input_dim = w.shape[0]

        self.transform = transform
        self.target_transform = target_transform

        if np_random is None:
            np_random = np.random.RandomState(None)

        if covariance is not None:
            assert covariance.shape[0] == self.input_dim
            covariance_matrix = covariance @ covariance.T
        else:
            covariance_matrix = np.eye(self.input_dim)

        if means is not None:
            assert means.shape[0] == self.input_dim
        else:
            means = torch.zeros(self.input_dim)

        # Scale the inputs i
        self._inputs = np_random.multivariate_normal(
            means, covariance_matrix, size=(num_samples, self.input_dim),
        )

        # self._targets = np.expand_dims(
        #     np_random.normal(size=(num_samples, self.input_dim)), 2
        # )
        self._targets = np.expand_dims(self._inputs @ w, 2)

        if (noise_std is not None) and (noise_std > 0.0):
            self._targets += noise_std * np_random.randn(num_samples, self.input_dim)

    def __len__(self):
        return self.num_samples

    def __getitem__(self, index):
        input, target = self._inputs[index], self._targets[index]

        if self.transform is not None:
            input = self.transform(input)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return (input, target, self.w.reshape(-1, 1))


def quadratics(shots, shuffle=True, test_shots=None, seed=None, **kwargs):
    """Helper function to create a meta-dataset for the Sinusoid toy dataset.
    Parameters
    ----------
    shots : int
        Number of (training) examples in each task. This corresponds to `k` in
        `k-shot` classification.
    shuffle : bool (default: `True`)
        Shuffle the examples when creating the tasks.
    test_shots : int, optional
        Number of test examples in each task. If `None`, then the number of test
        examples is equal to the number of training examples in each task.
    seed : int, optional
        Random seed to be used in the meta-dataset.
    kwargs
        Additional arguments passed to the `Sinusoid` class.
    See also
    --------
    `torchmeta.toy.Sinusoid` : Meta-dataset for the Sinusoid toy dataset.
    """
    if "num_samples_per_task" in kwargs:
        warnings.warn(
            "Both arguments `shots` and `num_samples_per_task` were "
            "set in the helper function for the number of samples in each task. "
            "Ignoring the argument `shots`.",
            stacklevel=2,
        )
        if test_shots is not None:
            shots = kwargs["num_samples_per_task"] - test_shots
            if shots <= 0:
                raise ValueError(
                    "The argument `test_shots` ({0}) is greater "
                    "than the number of samples per task ({1}). Either use the "
                    "argument `shots` instead of `num_samples_per_task`, or "
                    "increase the value of `num_samples_per_task`.".format(
                        test_shots, kwargs["num_samples_per_task"]
                    )
                )
        else:
            shots = kwargs["num_samples_per_task"] // 2
    if test_shots is None:
        test_shots = shots

    dataset = Quadratics(num_samples_per_task=shots + test_shots, **kwargs)
    dataset = ClassSplitter(
        dataset,
        shuffle=shuffle,
        num_train_per_class=shots,
        num_test_per_class=test_shots,
    )
    dataset.seed(seed)

    return dataset
