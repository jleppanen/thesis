#!/usr/bin/env python3

import math
import argparse
import time

import numpy as np
import torch

from torchmeta.utils.data import BatchMetaDataLoader

from optimizers import *
from helpers import plot_quadratic_path
from matplotlib import pyplot as plt
import os
from quadratic_experiment import Task, inner_loop

from tqdm import tqdm

# Code to produce the output required for the test loss plots in the quadratic experiment


def main():

    parser = argparse.ArgumentParser(description="Test loss calculation")
    parser.add_argument("--seed", type=int, default=5)
    parser.add_argument("--dataset", type=str, default="quad", help="dataset to use")
    parser.add_argument("--opt", type=str, default="all", help="optimiser architure")
    parser.add_argument("--dim", type=int, default=10, help="dimension")
    parser.add_argument("--T", type=int, default=100, help="number inner loop steps")
    parser.add_argument(
        "--structured",
        type=bool,
        default=False,
        help="draw data from non-standard normal",
    )
    parser.add_argument(
        "--reg-param", type=float, default=0.5, help="regularisation parameter"
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )
    parser.add_argument(
        "--constraint",
        type=str,
        default="strict",
        help="model constraint - strict/relaxed/none",
    )
    args = parser.parse_args()

    eval_bound = 0.98

    n_tasks_test = 1000
    reg_param = args.reg_param
    T = args.T

    loc = locals()
    del loc["parser"]
    del loc["args"]
    model_folder = (
        f"results/{args.constraint}/{args.dataset}_{args.dim}_{args.structured}_{T}"
    )
    print(args, "\n", loc, "\n")

    with open(os.path.join(model_folder, "test_setup.txt"), "w") as out_file:
        out_file.writelines([str(args), str(loc)])

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    if args.structured:
        cov_mat = torch.diag(torch.linspace(0.1, 0.5, args.dim))
        means = torch.linspace(0.5, 1.0, args.dim)
    else:
        cov_mat, means = None, None

    test_dataset = quadratics(
        1,
        num_tasks=n_tasks_test,
        dim=args.dim,
        covariance=cov_mat,
        means=means,
        seed=args.seed + 1,  # Ensure seed is not same as used in training
    )
    model = torch.nn.Linear(args.dim, 1, bias=False)
    test_dataloader = BatchMetaDataLoader(test_dataset, batch_size=n_tasks_test)

    if args.opt is "all":
        meta_models = []
        for file in os.listdir(model_folder):
            if file.startswith("model_"):
                meta_models.append(torch.load(os.path.join(model_folder, file)))
    else:
        meta_models = [
            torch.load(os.path.join(model_folder, f"model_{args.opt.upper()}"))
        ]

    inner_opt_kwargs = {"constraint": args.constraint}
    val_losses = torch.zeros(len(meta_models), n_tasks_test, args.T)
    experiment = {}
    f, ax = plt.subplots(1)
    for i, m in enumerate(meta_models):
        m.eval()

        val_losses = produce_plots(
            n_tasks_test,
            test_dataloader,
            model,
            m,
            args.T,
            m.get_optimiser,
            inner_opt_kwargs,
            args.reg_param,
        )
        ax.plot(
            val_losses.mean(dim=0), label=f"{repr(m)}",
        )
        ax.legend()
        ax.set_xlabel("t")
        ax.set_xlabel("Loss")
        experiment[f"{repr(m)}"] = val_losses
    f.suptitle(f"Mean test loss, {n_tasks_test} functions")
    f.savefig(f"test_paths/loss_comparison_data_quad_{args.dim}.png")
    for k, v in experiment.items():
        np.savetxt(
            f"{model_folder}/loss_comparison_data_quad_dim_{args.dim}_values_{k}.csv",
            v.numpy(),
        )


def produce_plots(
    n_tasks,
    dataloader,
    model,
    meta_model,
    n_steps,
    get_inner_opt,
    inner_opt_kwargs,
    reg_param,
):
    device = next(meta_model.parameters()).device
    val_losses = []
    for k, batch in enumerate(dataloader):
        tr_xs, tr_ys, tr_solns = (
            batch["train"][0].to(device),
            batch["train"][1].to(device),
            batch["train"][2],
        )
        tst_xs, tst_ys, tst_solns = (
            batch["test"][0].to(device),
            batch["test"][1].to(device),
            batch["test"][2],
        )

        for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in tqdm(
            enumerate(zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns))
        ):

            model.reset_parameters()
            task = Task(reg_param, model, (tr_x, tr_y, tst_x, tst_y))

            inner_opt = get_inner_opt(task.train_loss_f, **inner_opt_kwargs)
            params = [
                p.detach().clone().requires_grad_(True) for p in model.parameters()
            ]

            params_history, hx_history = inner_loop(
                [hp for hp in meta_model.parameters()],
                params,
                inner_opt,
                n_steps,
                log_interval=None,
            )
            losses = [
                task.val_loss_f(p, model.parameters()).item() for p in params_history
            ]

            val_losses.append(losses)

            if tr_x.shape[1] == 2 and t_idx == 0:
                f, ax = plt.subplots(1)
                plot_quadratic_path(
                    tr_x.float(), tr_y.float(), reg_param, params_history, ax,
                )
                # ax.plot(
                #     tr_soln.squeeze()[0], tr_soln.squeeze()[1], ".k", label="True soln"
                # )
                ax.legend()
                f.suptitle(f"Loss:{val_losses[-1][-1]}")
                f.savefig("test_paths/track_testing.png")
                plt.close(f)

        loss_tensor = torch.tensor(val_losses)
        return loss_tensor


if __name__ == "__main__":
    main()
