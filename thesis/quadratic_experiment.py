#!/usr/bin/env python3

import argparse
import time

import numpy as np
import torch
from torch.nn import functional as F

from torchmeta.utils.data import BatchMetaDataLoader

import higher

from optimizers import *
from helpers import plot_quadratic_path, fixed_point, list_to_tensor, reverse
from matplotlib import pyplot as plt
import os
from functools import reduce
import geoopt
import copy


# The main file for training optimisers in the quadratic experiment. See run_basic_models.sh for example shell commands for training.


class Task:
    """
    Ridge regression task for quadratic loss
    """

    def __init__(self, reg_param, model, data, batch_size=None):
        device = next(model.parameters()).device

        self.fmodel = higher.monkeypatch(
            model, device=device, copy_initial_weights=True
        )

        self.n_params = len(list(model.parameters()))
        self.train_input, self.train_target, self.test_input, self.test_target = [
            d.float() for d in data
        ]
        self.reg_param = reg_param
        self.batch_size = 1 if not batch_size else batch_size
        self.val_loss, self.val_acc = None, None

    def rr_reg_f(self, params):
        return sum([((p) ** 2).sum() for p in params])

    def train_loss_f(self, params, hparams):
        out = self.fmodel(self.train_input, params=params)
        return F.mse_loss(
            out, self.train_target
        ) + 0.5 * self.reg_param * self.rr_reg_f(params)

    def val_loss_f(self, params, hparams):
        out = self.fmodel(self.train_input, params=params)
        val_loss = (
            F.mse_loss(out, self.train_target) / self.batch_size
        )  # + 0.5 * self.reg_param * self.rr_reg_f(params)
        self.val_loss = val_loss.item()  # avoid memory leaks
        return val_loss


class Learner(torch.nn.Module):
    def __init__(self, input_size, output_size, *args, **kwargs):
        super(Learner, self).__init__()
        self.model = torch.nn.Linear(input_size, output_size, bias=False)

    def reset_parameters(self):
        for p in self.model.parameters():
            torch.nn.init.uniform_(p, -3, 3)

    def forward(self, *args, **kwargs):
        return self.model(*args, **kwargs)


def main():

    parser = argparse.ArgumentParser(
        description="Learning optimisers in multi-task setting"
    )
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument(
        "--hg-mode",
        type=str,
        default="fixed_point",
        metavar="N",
        help="hypergradient approximation: TBPTT or fixed_point",
    )
    parser.add_argument("--opt", type=str, default="lr", help="optimiser architure")
    parser.add_argument("--dataset", type=str, default="quad", help="dataset to use")
    parser.add_argument("--dim", type=int, default=10, help="dimensionality of data")
    parser.add_argument("--T", type=int, default=100, help="number inner loop steps")
    parser.add_argument("--ntasks", type=int, default=1000, help="number of tasks")
    parser.add_argument(
        "--structured",
        type=bool,
        default=False,
        help="draw data from non-standard normal",
    )
    parser.add_argument(
        "--componentwise", type=bool, default=False, help="train componentwise"
    )
    parser.add_argument(
        "--K", type=int, default=50, help="number of hg estimation steps"
    )
    parser.add_argument(
        "--reg-param", type=float, default=0.5, help="ridge regularisation parameter"
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    parser.add_argument(
        "--jacobian-param",
        type=float,
        default=None,
        help="local contractiveness penalty, none if not provided",
    )
    parser.add_argument(
        "--constraint",
        type=str,
        default="strict",
        help="model constraint - strict/relaxed/none",
    )

    args = parser.parse_args()

    log_interval = 10
    eval_interval = 10
    inner_log_interval = None
    inner_log_interval_test = None
    batch_size = 20
    n_tasks_test = 100
    T, K = args.T, args.K
    reg_param = args.reg_param
    eval_bound = 0.98
    compwise = args.componentwise
    constraint = args.constraint
    jacobian_param = args.jacobian_param

    T_test = T
    loc = locals()
    del loc["parser"]
    del loc["args"]

    print(args, "\n", loc, "\n")
    best_loss = 1e8
    best_model = None
    max_lipschitz = 10
    inner_opt_kwargs = {
        "T": T,
        "K": K,
        "reg_param": reg_param,
        "eval_bound": eval_bound,
        "L": max_lipschitz,
        "compwise": compwise,
        "constraint": constraint,
    }

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    if args.dataset == "quad":
        dim = args.dim
        if args.structured:
            cov_mat = torch.diag(torch.linspace(0.1, 0.5, dim))
            means = torch.linspace(0.5, 1.0, dim)
            print(cov_mat @ cov_mat.T)
        else:
            cov_mat, means = None, None

        dataset = quadratics(
            1,
            num_tasks=args.ntasks,
            dim=dim,
            seed=args.seed,
            covariance=cov_mat,
            means=means,
        )
        test_dataset = quadratics(
            1,
            num_tasks=n_tasks_test,
            dim=dim,
            seed=args.seed + 3,  # Ensure different data used for testing
            covariance=cov_mat,
            means=means,
        )
        model = Learner(dim, 1)
    else:
        raise NotImplementedError

    if args.opt == "sd":
        meta_model = SDMeta(dim, dim, **inner_opt_kwargs)
    elif args.opt == "crnn":
        meta_model = CRNNMeta(dim, dim, **inner_opt_kwargs)
    elif args.opt == "hcrnn":
        meta_model = HCRNNMeta(dim, dim, **inner_opt_kwargs)
    else:
        raise NotImplementedError

    dataloader = BatchMetaDataLoader(dataset, batch_size=batch_size, **kwargs)
    test_dataloader = BatchMetaDataLoader(test_dataset, batch_size=batch_size, **kwargs)

    if any([isinstance(p, geoopt.ManifoldParameter) for p in meta_model.parameters()]):
        outer_opt = geoopt.optim.RiemannianAdam(params=meta_model.parameters())
        manifold_opt = True
    else:
        outer_opt = torch.optim.Adam(params=meta_model.parameters())
        manifold_opt = False

    def get_inner_opt(train_loss, **inner_opt_kwargs):
        opt = meta_model.get_optimiser(train_loss, **inner_opt_kwargs)
        return opt

    hparams_history = [[p.data.clone() for p in meta_model.parameters()]]
    test_losses = []
    tr, ta = None, None
    for k, batch in enumerate(dataloader):
        start_time = time.time()
        meta_model.train()

        tr_xs, tr_ys, tr_solns = (
            batch["train"][0].to(device),
            batch["train"][1].to(device),
            batch["train"][2],
        )
        tst_xs, tst_ys, tst_solns = (
            batch["test"][0].to(device),
            batch["test"][1].to(device),
            batch["test"][2],
        )

        val_loss = 0
        forward_time, backward_time = 0, 0
        outer_opt.zero_grad()
        for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in enumerate(
            zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns)
        ):

            model.reset_parameters()
            start_time_task = time.time()
            task = Task(
                reg_param, model, (tr_x, tr_y, tst_x, tst_y), batch_size=tr_xs.shape[0],
            )

            # Calculate largest eigenvalue of batch in order to estimate the Lipschitz constant
            max_evalue = reduce(
                max,
                map(
                    lambda t: max(torch.eig(t)[0][:, 0]),
                    tr_x.transpose(1, 2) @ tr_x + reg_param * torch.eye(tr_x.shape[1]),
                ),
            )
            max_lipschitz = max(max_lipschitz, 2 / tr_y.shape[1] * max_evalue)

            inner_opt = get_inner_opt(task.train_loss_f, L=max_lipschitz)
            params_history, hx_history = inner_loop(
                [hp for hp in meta_model.parameters()],
                [p for p in model.parameters()],
                inner_opt,
                T,
                log_interval=inner_log_interval,
            )
            if torch.isinf(params_history[-1][0]).any():
                pdb.set_trace()
            last_param = params_history[-1]
            forward_time_task = time.time() - start_time_task

            if args.hg_mode == "fixed_point":
                hg_val = fixed_point(
                    last_param,
                    hx_history[-1],
                    [hp for hp in meta_model.parameters()],
                    K,
                    inner_opt,
                    task.val_loss_f,
                    jacobian_param=jacobian_param,
                )
                if torch.isnan(hg_val[0]).any():
                    pdb.set_trace()
            elif args.hg_mode == "reverse":
                hg_val = reverse(
                    last_param,
                    hx_history[-K - 1 :],
                    [hp for hp in meta_model.parameters()],
                    K,
                    inner_opt,
                    task.val_loss_f,
                )
            elif args.hg_mode == "test":
                hg_val_r = reverse(
                    last_param,
                    hx_history[-K - 1 :],
                    [hp for hp in meta_model.parameters()],
                    K,
                    inner_opt,
                    task.val_loss_f,
                    set_grad=False,
                )
                o_loss = task.val_loss_f(
                    last_param, [hp for hp in meta_model.parameters()]
                )
                grads = torch.autograd.grad(
                    o_loss, [hp for hp in meta_model.parameters()], retain_graph=True
                )
                hg_val_fp = fixed_point(
                    last_param,
                    hx_history[-1],
                    [hp for hp in meta_model.parameters()],
                    K,
                    inner_opt,
                    task.val_loss_f,
                    jacobian_penalised=False,
                    set_grad=False,
                )
                pdb.set_trace()
            else:
                raise NotImplementedError
            backward_time_task = time.time() - start_time_task - forward_time_task
            val_loss += task.val_loss
            forward_time += forward_time_task
            backward_time += backward_time_task

        outer_opt.step()
        hparams_history.append([p.data.clone() for p in meta_model.parameters()])
        step_time = time.time() - start_time

        if k % log_interval == 0:
            print(
                "MT k={} ({:.3f}s F: {:.3f}s, B: {:.3f}s) Val Loss: {:.2e}.".format(
                    k, step_time, forward_time, backward_time, val_loss,
                )
            )
            if dim < 5:
                print(f"Hparams={[(p, p.grad) for p in meta_model.parameters()]}")
                print(1 / max_lipschitz)
            else:
                print(
                    f"Hypergrad norms {sum([torch.sum(p.grad) for p in meta_model.parameters()]).sum()}"
                )

        if k % eval_interval == 0:
            test_loss = evaluate(
                n_tasks_test,
                test_dataloader,
                model,
                meta_model,
                T_test,
                get_inner_opt,
                {},
                reg_param,
                log_interval=inner_log_interval_test,
                full_path=True,
            )
            test_losses.append(test_loss.mean(axis=0))
            meta_model.train()

            print(
                "Test loss {:.2e} +- {:.2e}.".format(
                    test_loss.mean(axis=0)[-1],
                    test_loss.std(axis=0)[-1],
                    len(test_loss),
                )
            )

        if test_loss.mean(axis=0)[-1] < best_loss:
            if manifold_opt:
                best_hparams = [
                    copy.deepcopy(torch.nn.Parameter(p))
                    for p in meta_model.parameters()
                ]
            else:
                best_model = copy.deepcopy(meta_model)
            best_loss = test_loss.mean(axis=0)[-1]

    out_folder = f"results/{args.constraint}/{args.dataset}_{dim}_{args.structured}_{T}"

    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    torch.save(
        list_to_tensor(hparams_history), f"{out_folder}/hparams_{repr(meta_model)}",
    )

    np.savetxt(
        f"{out_folder}/train_eval_losses_{repr(meta_model)}.csv", np.stack(test_losses)
    )
    if manifold_opt:
        print(best_hparams, max_lipschitz)
        for i, p in enumerate(meta_model.parameters()):
            p = best_hparams[i]
        best_model = meta_model
    torch.save(
        best_model, f"{out_folder}/model_{repr(meta_model)}",
    )
    with open(os.path.join(out_folder, "train_setup.txt"), "w") as out_file:
        out_file.writelines([str(args), str(loc)])


def inner_loop(hparams, params, optim, n_steps, log_interval, create_graph=False):
    params_history = [params]
    hx_history = [[p.detach().clone().requires_grad_(True) for p in params]]
    for t in range(n_steps):
        output, new_hx = optim(
            params_history[-1], hx_history[-1], hparams, create_graph=create_graph
        )
        params_history.append(output)
        hx_history.append(new_hx)
        if log_interval and (t % log_interval == 0 or t == n_steps - 1):
            print("t={}, Loss: {:.6f}".format(t, optim.curr_loss.item()))

    return params_history, hx_history


def evaluate(
    n_tasks,
    dataloader,
    model,
    meta_model,
    n_steps,
    get_inner_opt,
    inner_opt_kwargs,
    reg_param,
    log_interval=None,
    full_path=False,
):
    meta_model.eval()
    device = next(meta_model.parameters()).device

    val_losses = []
    for k, batch in enumerate(dataloader):
        tr_xs, tr_ys, tr_solns = (
            batch["train"][0].to(device),
            batch["train"][1].to(device),
            batch["train"][2],
        )
        tst_xs, tst_ys, tst_solns = (
            batch["test"][0].to(device),
            batch["test"][1].to(device),
            batch["test"][2],
        )

        for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in enumerate(
            zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns)
        ):

            model.reset_parameters()
            task = Task(reg_param, model, (tr_x, tr_y, tst_x, tst_y))

            inner_opt = get_inner_opt(task.train_loss_f, **inner_opt_kwargs)
            params = [
                p.detach().clone().requires_grad_(True) for p in model.parameters()
            ]

            params_history, hx_history = inner_loop(
                [hp for hp in meta_model.parameters()],
                params,
                inner_opt,
                n_steps,
                log_interval=log_interval,
            )
            last_param = params_history[-1]

            if full_path:
                losses = [
                    task.val_loss_f(p, model.parameters()).item()
                    for p in params_history
                ]
                val_losses.append(losses)
            else:
                task.val_loss_f(last_param, meta_model.parameters())
                val_losses.append(task.val_loss)

            if tr_x.shape[1] == 2 and t_idx == 0:
                f, ax = plt.subplots(1)
                plot_quadratic_path(
                    tr_x.float(), tr_y.float(), reg_param, params_history, ax,
                )

                # ax.plot(
                #     tr_soln.squeeze()[0], tr_soln.squeeze()[1], ".k", label="True soln"
                # )
                ax.legend()
                # f.suptitle(f"{tr_x.data}")
                f.savefig("test_paths/track_training.png")
                plt.close(f)

            if len(val_losses) >= n_tasks:
                return np.array(val_losses)


if __name__ == "__main__":
    main()
