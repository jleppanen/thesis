#!/usr/bin/env python3
import argparse
import time

import numpy as np
import torch

from torchmeta.utils.data import BatchMetaDataLoader

from optimizers import *
from helpers import fixed_point, reverse
from matplotlib import pyplot as plt
from quadratic_experiment import Task, inner_loop
from tqdm import tqdm
from functools import reduce


# Produces the data for the fixed point convergence plots in Appendix B


def main():

    parser = argparse.ArgumentParser(
        description="Learning optimisers in multi-task setting"
    )
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument(
        "--hg-mode",
        type=str,
        default="fixed_point",
        metavar="N",
        help="hypergradient approximation: TBPTT or fixed_point",
    )
    parser.add_argument("--opt", type=str, default="crnn", help="optimiser architure")
    parser.add_argument("--dataset", type=str, default="quad", help="dataset to use")
    parser.add_argument("--dim", type=int, default=10, help="dimensionality of data")
    parser.add_argument("--T", type=int, default=100, help="number inner loop steps")
    parser.add_argument("--ntasks", type=int, default=1000, help="number of tasks")
    parser.add_argument(
        "--structured",
        type=bool,
        default=False,
        help="draw data from non-standard normal",
    )
    parser.add_argument(
        "--componentwise", type=bool, default=False, help="train componentwise"
    )
    parser.add_argument(
        "--K", type=int, default=40, help="number of hg estimation steps"
    )
    parser.add_argument(
        "--reg-param", type=float, default=0.0, help="ridge regularisation parameter"
    )
    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    args = parser.parse_args()

    batch_size = 10
    n_tasks_test = 10
    T = args.T
    reg_param = args.reg_param
    eval_bound = 0.98
    activation = torch.tanh
    compwise = args.componentwise
    T_test = T

    loc = locals()
    del loc["parser"]
    del loc["args"]

    print(args, "\n", loc, "\n")

    max_lipschitz = 10
    inner_opt_kwargs = {
        "T": T,
        "reg_param": reg_param,
        "eval_bound": eval_bound,
        "activation": activation,
        "L": max_lipschitz,
        "compwise": compwise,
    }

    cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    kwargs = {"num_workers": 1, "pin_memory": True} if cuda else {}

    torch.random.manual_seed(args.seed)
    np.random.seed(args.seed)

    dim = args.dim
    dataset = quadratics(1, num_tasks=n_tasks_test, dim=dim, seed=args.seed)
    model = torch.nn.Linear(dim, 1, bias=False)

    if args.opt == "lr":
        meta_model = SDMeta(dim, dim, **inner_opt_kwargs)
    elif args.opt == "crnn":
        meta_model = CRNNMeta(dim, dim, **inner_opt_kwargs)
        meta_model.Sh = torch.nn.Parameter(torch.ones(args.dim) * eval_bound)
    elif args.opt == "hcrnn":
        meta_model = HCRNNMeta(dim, dim, **inner_opt_kwargs)
    else:
        raise NotImplementedError

    dataloader = BatchMetaDataLoader(dataset, batch_size=batch_size, **kwargs)

    def get_inner_opt(train_loss, **inner_opt_kwargs):
        opt = meta_model.get_optimiser(train_loss, **inner_opt_kwargs)
        return opt

    for k, batch in enumerate(dataloader):
        start_time = time.time()
        meta_model.train()

        tr_xs, tr_ys, tr_solns = (
            batch["train"][0].to(device),
            batch["train"][1].to(device),
            batch["train"][2],
        )
        tst_xs, tst_ys, tst_solns = (
            batch["test"][0].to(device),
            batch["test"][1].to(device),
            batch["test"][2],
        )

        iter_grid = torch.range(1, 100)
        hg_vals = torch.zeros(
            2,
            iter_grid.shape[0],
            sum([p.numel() for p in meta_model.parameters()]) // 2,
        )
        for i, K in tqdm(enumerate(iter_grid)):
            for t_idx, (tr_x, tr_y, tr_soln, tst_x, tst_y, tst_soln) in enumerate(
                zip(tr_xs, tr_ys, tr_solns, tst_xs, tst_ys, tst_solns)
            ):
                model.reset_parameters()

            start_time_task = time.time()
            task = Task(
                reg_param, model, (tr_x, tr_y, tst_x, tst_y), batch_size=tr_xs.shape[0],
            )

            # Calculate largest eigenvalue of batch in order to estimate the Lipschitz constant
            max_evalue = reduce(
                max,
                map(lambda t: max(torch.eig(t)[0][:, 0]), tr_x.transpose(1, 2) @ tr_x,),
            )
            max_lipschitz = max(max_lipschitz, 2 / tr_y.shape[1] * max_evalue)

            inner_opt = get_inner_opt(task.train_loss_f, L=max_lipschitz)
            params_history, hx_history = inner_loop(
                [hp for hp in meta_model.parameters()],
                [p for p in model.parameters()],
                inner_opt,
                T,
                log_interval=None,
            )

            last_param = params_history[-1]
            forward_time_task = time.time() - start_time_task

            if K == 1:
                o_loss = task.val_loss_f(
                    last_param, [hp for hp in meta_model.parameters()]
                )
                true_grads = torch.autograd.grad(
                    o_loss, [hp for hp in meta_model.parameters()], retain_graph=True
                )
                true_grads = torch.cat(
                    tuple(map(lambda x: x.reshape(-1, 2), true_grads))
                ).reshape(2, -1)

            if args.hg_mode == "fixed_point":
                hg_val = fixed_point(
                    last_param,
                    hx_history[-1],
                    [hp for hp in meta_model.parameters()],
                    int(K),
                    inner_opt,
                    task.val_loss_f,
                    jacobian_penalised=False,
                )
            elif args.hg_mode == "reverse":
                hg_val = reverse(
                    last_param,
                    hx_history[-int(K) - 1 :],
                    [hp for hp in meta_model.parameters()],
                    int(K),
                    inner_opt,
                    task.val_loss_f,
                )
            else:
                raise NotImplementedError
            hg_vals[:, i, :] += torch.cat(
                tuple(map(lambda x: x.reshape(-1, 2), hg_val))
            ).reshape(2, -1)
    hg_tensor = hg_vals
    num_parameters = len(
        [p.numel() for p in meta_model.parameters() if p.requires_grad]
    )
    f, ax = plt.subplots(1, 2)
    experiment = {"k": iter_grid, "true_hg": true_grads}
    for i, n in enumerate(["W", "U"]):
        experiment[n] = hg_tensor[i, :, :]
        ax[i].plot(iter_grid, hg_tensor[i, :, :])
        ax[i].set_xlabel("Iteration")
        ax[i].set_ylabel("Hypergradient value")
        ax[i].set_title(f"Hypergradient of {n}")
    f.suptitle("Convergence of the fixed point method")
    f.savefig(f"visualisations/{args.opt}_{args.hg_mode}_hg_fp_convergence.png")
    torch.save(
        experiment, f"results/hg_fp_convergence_opt_{args.opt}_{args.hg_mode}_dim_{dim}"
    )
    for k, v in experiment.items():
        np.savetxt(
            f"results/hg_fp_opt_{args.opt}_{args.hg_mode}_dim_{dim}_values_{k}.csv",
            v.numpy(),
        )


if __name__ == "__main__":
    main()
