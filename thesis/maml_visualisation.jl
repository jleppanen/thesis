using CSV
using Plots
using LaTeXStrings

# Code to produce the sample sinusoid plot in Chapter 3. Assumes maml_visualisation.py has been run such that the required data exists in the ./results/maml/plot_data/ folder.

cd("./results/maml/plot_data/")

nplots = length(filter(file -> occursin("inputs_",file), readdir()))

amplitudes = convert(Matrix, CSV.read("amplitudes.csv", header=false))
phases = convert(Matrix, CSV.read("phases.csv", header=false))


gr()
plots = typeof(plot())[]
for plot_idx in 1:nplots
    inputs = convert(Matrix, CSV.read("inputs_data_"*string(plot_idx-1)*".csv", header=false))
    predictions = convert(Matrix, CSV.read("plot_data_"*string(plot_idx-1)*".csv", header=false))
    x_label = if plot_idx > 3 L"x" else "" end
    y_label = if plot_idx%3 == 1 L"y" else "" end
    p = plot(predictions[1,:], predictions[2:end,:]',xlim=(-5,5), ylim=(-5,5), yticks=(plot_idx%3==1),xticks=(plot_idx>3), grid=false, linestyle=[:solid], xlabel=x_label, ylabel=y_label)
    p = plot!(inputs[1,:], inputs[2,:], seriestype=:scatter, label="Inputs", ylabel=y_label)
    p = plot!(predictions[1,:], amplitudes[plot_idx].*sin.(predictions[1,:] .- phases[plot_idx]), label="Oracle", legend=false, linestyle=:dot, linecolor=:red)
    push!(plots, p)
    if plot_idx%3 == 0
        p2 = plot(predictions[1,:], predictions[2:end,:]',label=[L"\theta_0 \textrm{ SD-c}" L"\theta_1 \textrm{ SD-c}" L"\theta_0 \textrm{ CRNN-c}" L"\theta_1 \textrm{ CRNN-c}"],xlim=(-5,5), ylim=(-5,5), yticks=(plot_idx%3==1),xticks=(plot_idx>3), grid=false, linestyle=[:solid])
        p2 = plot!(inputs[1,:], inputs[2,:], seriestype=:scatter, label="Inputs")
        p2 = plot!(predictions[1,:], amplitudes[plot_idx].*sin.(predictions[1,:] .- phases[plot_idx]), label="Oracle", legend=false, linecolor=:red, linestyle=:dot, linealpha=0.8)
        p2 = plot!(grid=false, showaxis=false, legend=plot_idx==3, xlim=(-100,-99), ylim=(-100,-99))
        push!(plots, p2)
    end
end
l = @layout [a b c d{0.3w} ; e  f  g  h{0.3w}]
plot(plots..., layout=l, dpi=300, title=L"n_{shots}=10, \ L_\nabla = 10")
savefig("/home/juha/code/Thesis/thesis/visualisations/maml_sin.png")
